var express = require('express');
var router = express.Router();
var config = require('../config');

var OrderRepo = require('../repositories/orderRepository');
var FactoryRepo = require('../repositories/factoryRepository');

const request = require('request');

// GET (ccessed at GET http://localhost:8080/orderManager/calcCircuit)
router.get('/calcCircuit', async function (req, res, next) {
    var readyToShipOrders = await OrderRepo.findOrdersByState('READY_TO_SHIP').catch(function(error) {
        console.log(error);
    });
    var uri = 'https://' + config.urlSiC_R + '/calcCircuit';
    var options = {
        uri: uri,
        method: 'POST',
        headers: { 'Content-Type': "application/json" },
        json: true,
        body : { "orders" : readyToShipOrders }
    };

    request.post(uri, options, function (error, response, body) {
        res.send(body);
        console.log("Response from SiC_R: " + JSON.stringify(body));
    });
});

// GET (accessed at GET http://localhost:8080/orderManager/assignOrders)
router.get('/assignOrders', async function (req, res, next) {    
    var factories = await FactoryRepo.findAllFactories().catch(function(error) {
        console.log(error);
    });
    
    var submittedOrders = await OrderRepo.findOrdersByState('SUBMITTED').catch(function(error) {
        console.log(error);
    });

    var uri = 'https://' + config.urlSiC_R + '/assignOrders';
    //uri = 'http://localhost:8080' + '/assignOrders';
    var options = {
        uri: uri,
        method: 'POST',
        headers: { 'Content-Type': "application/json" },
        json: true,
        body : {
                "orders" : submittedOrders,
                "factories" : factories
        }
    };

    console.log("Request sending to SiC_R: " + JSON.stringify(options.body));
    request.post(uri, options, function (error, response, body) {
        res.send(body);
        console.log("Response from SiC_R: " + JSON.stringify(body));
    });
});

module.exports = router;