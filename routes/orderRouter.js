var express = require('express');
var router = express.Router();
var Order = require('../models/order');
var firebaseMiddleware = require('express-firebase-middleware');

var OrderRepo = require('../repositories/orderRepository');

// GET all orders (accessed at GET http://localhost:8080/orders)
router.get('/',  firebaseMiddleware.auth, function (req, res, next) {
  Order.find(function (err, orders) {
    if (err)
      res.send(err);

    res.json(orders);
  });
});

// create a order (accessed at POST http://localhost:8080/orders)
router.post('/', firebaseMiddleware.auth, function (req, res) {

  var order = new Order();      // create a new instance of the Material model
  order.orderItems = req.body.orderItems;
  order.datePlaced = req.body.datePlaced;
  order.state = req.body.state;
  order.factory = req.body.factory;
  order.deliveryAddress = req.body.deliveryAddress;
  order.customer = req.body.customer;

  // save the order and check for errors
  order.save(function (err) {
    if (err)
      res.send(err);

    res.json({ message: 'Order created!' });
  });

});


// on routes that end in /orders/:order_id
// ----------------------------------------------------
router.route('/:order_id')

  // get the order with that id (accessed at GET http://localhost:8080/orders/:order_id)
  .get(firebaseMiddleware.auth, function (req, res) {
    Order.findById(req.params.order_id, function (err, order) {
      if (err)
        res.send(err);
      res.json(order);
    });
  });

router.route('/:order_id')

  // update the order with this id (accessed at PUT http://localhost:8080/orders/:order_id)
  .put(firebaseMiddleware.auth, function (req, res) {

    Order.findById(req.params.order_id, function (err, order) {

      if (err)
        res.send(err);

      order.orderItems = req.body.orderItems;
      order.datePlaced = req.body.datePlaced;
      order.state = req.body.state;
      order.factory = req.body.factory;
      order.deliveryAddress = req.body.deliveryAddress;
      order.customer = req.body.customer;

      // save the order
      order.save(function (err) {
        if (err)
          res.send(err);

        res.json({ message: 'Order updated!' });
      });

    });
  });

// on routes that end in /orders/:order_id
// ----------------------------------------------------
router.route('/:order_id')

  // delete the order with this id (accessed at DELETE http://localhost:8080/orders/:order_id)
  .delete(firebaseMiddleware.auth, function (req, res) {
    Order.remove({
      _id: req.params.order_id
    }, function (err, order) {
      if (err)
        res.send(err);

      res.json({ message: 'Successfully deleted' });
    });
  });



// GET order history of a customer (accessed at POST http://localhost:8080/orderHistory)
router.get('/orderHistory/:customer_id', async function (req, res, next) {
  var orderHistory = await OrderRepo.findByCustomer(req.params.customer_id);
  
  res.json(orderHistory);
});


// GET order history of a customer filter by state (accessed at POST http://localhost:8080/orderHistory)
router.get('/orderHistory/:customer_id/:state', async function (req, res, next) {
  var orderHistory = await OrderRepo.findByCustomerAndState(req.params.customer_id, req.params.state);

  res.json(orderHistory);
});



// GET order history of a customer filter by a date interval (accessed at POST http://localhost:8080/orderHistory)
router.get('/orderHistory/:customer_id/:date_init/:date_end', async function (req, res, next) {
  var init = new Date(req.params.date_init);
  var end = new Date (req.params.date_end);
  var orderHistory = await OrderRepo.findByCustomerAndDateInterval(req.params.customer_id,init,end);

  res.json(orderHistory);
});


module.exports = router;