var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
//const AutoIncrement = require('mongoose-sequence')(mongoose);


var RouteSchema   = new Schema({
    //Every Route starts (and ends) at the same Factory
    origin : { type: Schema.Types.Object, ref: 'Factory' },
    //The total distance travelled
    totalDistance : Number,
    //The path is a sorted sequence of Orders (each Order has an Address and Location)
    path : [{ type: Schema.Types.Object, ref: 'Order' }],
});

RouteSchema.methods.pathAsLocations = function pathAsLocations () {
    result = [];
        
    //The path starts at the Factory/origin
    result.push(this.origin.location);

    //Get the location of the city of the address of each Order
    for (i = 0; i < this.path.length; i++) {
        result.push(this.path[i].address.city.location);
    }

    /*Don't forget that the Truck must return to the origin,
    otherwise it wouldn't be a circuit :P*/
    result.push(this.origin.location);

    return result;
};

RouteSchema.plugin(autoIncrement.autoIncrement,{
    model:'Route',
    startAt: 1
});

module.exports = mongoose.model('Route', RouteSchema);