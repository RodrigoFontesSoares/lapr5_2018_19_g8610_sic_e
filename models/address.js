var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
//const AutoIncrement = require('mongoose-sequence')(mongoose);


var AddressSchema   = new Schema({
    city : { type: Schema.Types.Object, ref: 'City' },
    //We have no need for the actual fullAddress, but record it anyway
    fullAddress : String
});

AddressSchema.plugin(autoIncrement.autoIncrement,{
    model:'Address',
    startAt: 1
});

module.exports = mongoose.model('Address', AddressSchema);