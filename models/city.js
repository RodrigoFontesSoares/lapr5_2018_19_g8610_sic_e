var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
//const AutoIncrement = require('mongoose-sequence')(mongoose);


var CitySchema   = new Schema({
    name : string,
    location : { type: Schema.Types.Object, ref: 'Location' }
});

CitySchema.plugin(autoIncrement.autoIncrement,{
    model:'City',
    startAt: 1
});

module.exports = mongoose.model('City', CitySchema);