var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
//const AutoIncrement = require('mongoose-sequence')(mongoose);


var CustomerSchema   = new Schema({
    /**
     * This is NOT the default Mongo Id. Mongo generates an id for each Customer OBJECT,
     * but this ID represents Customers as PEOPLE. E.g. if I am Customer "123ABC"
     * and I place Order #345, that Order might have a "Customer.id = 3" but a
     * "Customer.customer_id = 123ABC". If I then place Order #346, that Order will have
     * "Customer.id = 4" BUT "Customer.customer_id" remains "123ABC". The point is to
     * have a common attribute amongst Orders that links them to the same Customer
     */
    customer_id : String
});

CustomerSchema.plugin(autoIncrement.autoIncrement,{
    model:'Customer',
    startAt: 1
});

module.exports = mongoose.model('Customer', CustomerSchema);