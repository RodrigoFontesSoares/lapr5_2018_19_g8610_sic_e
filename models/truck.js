var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
//const AutoIncrement = require('mongoose-sequence')(mongoose);


var TruckSchema   = new Schema({
    name : String
});

TruckSchema.plugin(autoIncrement.autoIncrement,{
    model:'Truck',
    startAt: 1
});

module.exports = mongoose.model('Truck', TruckSchema);