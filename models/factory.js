var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
//const AutoIncrement = require('mongoose-sequence')(mongoose);


var FactorySchema   = new Schema({
    name : String,
    location : { type: Schema.Types.Object, ref: 'Location' }
});

FactorySchema.plugin(autoIncrement.autoIncrement,{
    model:'Factory',
    startAt: 1
});

module.exports = mongoose.model('Factory', FactorySchema);