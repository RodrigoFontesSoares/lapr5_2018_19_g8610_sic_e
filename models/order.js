var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);

var OrderSchema = new Schema({
    orderItems: [{ type: Schema.Types.Object, ref: 'ProductItem' }],
    datePlaced: Date,
    state: {
        type: String,
        enum: ['SUBMITTED', 'VALIDATED', 'ASSIGNED', 'IN_PRODUCTION',
            'PACKING', 'READY_TO_SHIP', 'SHIPPED', 'DELIVERED'],
        default: ['SUBMITTED']
    },
    factory: { type: Schema.Types.Object, ref: 'Factory' },
    deliveryAddress: { type: Schema.Types.Object, ref: 'Address' },
    customer: String
});

OrderSchema.plugin(autoIncrement.autoIncrement, {
    model: 'Order',
    startAt: 1
});

module.exports = mongoose.model('Order', OrderSchema);