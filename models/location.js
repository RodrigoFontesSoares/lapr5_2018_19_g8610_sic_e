var mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autoIncrement = require('mongoose-plugin-autoinc');
mongoose.set('useCreateIndex', true);
mongoose.set('useFindAndModify', false);
//const AutoIncrement = require('mongoose-sequence')(mongoose);


var LocationSchema   = new Schema({
    latitude : Number,
    longitude : Number
});

LocationSchema.plugin(autoIncrement.autoIncrement,{
    model:'Location',
    startAt: 1
});

module.exports = mongoose.model('Location', LocationSchema);