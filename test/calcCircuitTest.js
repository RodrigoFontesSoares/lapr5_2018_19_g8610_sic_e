var app = require('../app');
let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();
chai.use(chaiHttp);

describe('GET calcCircuit', () => {
    it('it should GET correct Circuit', (done) => {
        chai.request(app)
        .get('/orderManager/calcCircuit')
        .end((err, res) => {
            res.status.should.be.equal(200);
            res.body.distance.should.be.equal(15267498);
            done();
        });
    });
});

// describe("CalcCircuit", function () {
//     it ('should fail because not all orders are ready to order', function (done) {
//         var input = {
//             "orders": [
//                 {
//                     "orderItems": [],
//                     "state": "SUBMITTED",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "brussels",
//                             "location": {
//                                 "latitude": 50.8462807,
//                                 "	longitude": 4.3547273
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "20"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "tirana",
//                             "location": {
//                                 "latitude": 41.33165,
//                                 "longitude": 19.8318
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "london",
//                             "location": {
//                                 "latitude": 51.5001524,
//                                 "longitude": -0.1262362
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "rome",
//                             "location": {
//                                 "latitude": 41.8954656,
//                                 "longitude": 12.4823243
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "skopje",
//                             "location": {
//                                 "latitude": 42.003812,
//                                 "longitude": 21.452246
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "warsaw",
//                             "location": {
//                                 "latitude": 52.2296756,
//                                 "longitude": 21.0122287
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "bucharest",
//                             "location": {
//                                 "latitude": 44.430481,
//                                 "longitude": 26.12298
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 }, {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "moscow",
//                             "location": {
//                                 "latitude": 55.755786,
//                                 "longitude": 37.617633
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "edinburgh",
//                             "location": {
//                                 "latitude": 55.9501755,
//                                 "longitude": -3.1875359
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "belgrade",
//                             "location": {
//                                 "latitude": 44.802416,
//                                 "longitude": 20.465601
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "ljubljana",
//                             "location": {
//                                 "latitude": 46.0514263,
//                                 "longitude": 14.5059655
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "stockholm",
//                             "location": {
//                                 "latitude": 59.3327881,
//                                 "longitude": 18.0644881
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "bern",
//                             "location": {
//                                 "latitude": 46.9479986,
//                                 "longitude": 7.4481481
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "cardiff",
//                             "location": {
//                                 "latitude": 51.4813069,
//                                 "longitude": -3.1804979
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "monaco",
//                             "location": {
//                                 "latitude": 43.750298,
//                                 "longitude": 7.412841
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "luxembourg",
//                             "location": {
//                                 "latitude": 49.815273,
//                                 "longitude": 6.129583
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "dublin",
//                             "location": {
//                                 "latitude": 53.344104,
//                                 "longitude": -6.2674937
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "paris",
//                             "location": {
//                                 "latitude": 48.8566667,
//                                 "longitude": 2.3509871
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "porto",
//                         "location": {
//                             "latitude": 41.1621376,
//                             "longitude": -8.6569732
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "athens",
//                             "location": {
//                                 "latitude": 37.97918,
//                                 "longitude": 23.716647
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "athens",
//                         "location": {
//                             "latitude": 37.97918,
//                             "longitude": 23.716647
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "riga",
//                             "location": {
//                                 "latitude": 56.9465346,
//                                 "longitude": 24.1048525
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "riga",
//                         "location": {
//                             "latitude": 56.9465346,
//                             "longitude": 24.1048525
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "sarajevo",
//                             "location": {
//                                 "latitude": 43.85643,
//                                 "longitude": 18.41342
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "sarajevo",
//                         "location": {
//                             "latitude": 43.85643,
//                             "longitude": 18.41342
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "vaduz",
//                             "location": {
//                                 "latitude": 47.1410409,
//                                 "longitude": 9.5214458
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "sarajevo",
//                         "location": {
//                             "latitude": 43.85643,
//                             "longitude": 18.41342
//                         }
//                     },
//                     "customer": "21"
//                 },
//                 {
//                     "orderItems": [],
//                     "state": "READY_TO_SHIP",
//                     "datePlaced": "2018-12-20T00:00:00.000Z",
//                     "deliveryAddress": {
//                         "city": {
//                             "name": "tbilisi",
//                             "location": {
//                                 "latitude": 41.709981,
//                                 "longitude": 44.792998
//                             }
//                         }
//                     },
//                     "factory": {
//                         "name": "sarajevo",
//                         "location": {
//                             "latitude": 43.85643,
//                             "longitude": 18.41342
//                         }
//                     },
//                     "customer": "21"
//                 }
//             ],
//             "trucks": [

//             ]
//         }
//         var expectedResult = {
//             "distance": 15267498,
//             "circuit": "{\"porto\",\"paris\",\"monaco\",\"rome\",\"skopje\",\"tirana\",\"athens\",\"tbilisi\",\"moscow\",\"stockholm\",\"riga\",\"warsaw\",\"bucharest\",\"belgrade\",\"sarajevo\",\"ljubljana\",\"vaduz\",\"bern\",\"luxembourg\",\"brussels\",\"london\",\"cardiff\",\"edinburgh\",\"dublin\",\"porto\"}"
//         }
//         var uri = 'https://' + config.urlSiC_R + '/calcCircuit';
//         var options = {
//             uri: uri,
//             method: 'POST',
//             headers: { 'Content-Type': "application/json" },
//             json: true,
//             body: { "orders": input }
//         };
//         // var response;
//         // request.post(uri, options, function (error, response, body) {
//         //     response=body;
            
//         // });
//         // assert.notEqual(JSON.stringify(response), expectedResult);       
        
//         request(calc)
//         .get('/calcCircuit')
//         .expect('Content-Type', /json/)
//         .expect(200, {
//             distance: 15267498,
//             circuit: "{\"porto\",\"paris\",\"monaco\",\"rome\",\"skopje\",\"tirana\",\"athens\",\"tbilisi\",\"moscow\",\"stockholm\",\"riga\",\"warsaw\",\"bucharest\",\"belgrade\",\"sarajevo\",\"ljubljana\",\"vaduz\",\"bern\",\"luxembourg\",\"brussels\",\"london\",\"cardiff\",\"edinburgh\",\"dublin\",\"porto\"}"
//         }, done);
//     });
// });


