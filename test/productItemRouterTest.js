//For InMemory Mongoose database
const mongoose = require('mongoose');
const MongoMemoryServer = require('mongodb-memory-server');

var app = require('../app');

//Chai is an HTTP client appropriate for tests 
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

const firebaseConfig = require('../firebaseConfig');
const firebaseService = require('./firebaseService');

firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testPostItemProduct);
function testPostItemProduct(token) {
    describe('POST ItemProduct', () => {
        it('should create an ItemProduct', (done) => {
            var order = {
                "orderItems": [],
                "state": "SUBMITTED",
                "datePlaced": "2018-12-20T00:00:00.000Z",
                "deliveryAddress": {
                    "city": {
                        "name": "brussels",
                        "location": {
                            "latitude": 50.8462807,
                            "longitude": 4.3547273
                        }
                    }
                },
                "factory": {
                    "name": "porto",
                    "location": {
                        "latitude": 41.1621376,
                        "longitude": -8.6569732
                    }
                },
                "customer": "20"
            };

            chai.request(app)
                .post('/productItems/')
                .set('Authorization', 'Bearer ' + token)
                .send(order)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });

}

firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testGetItemProduct);
function testGetItemProduct(token) {
    describe('GET productItem', function () {
        it('should get an productItem', function (done) {
            chai.request(app)
                .get('/productItems/1')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });
}

firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testGetAllItemProducts);
function testGetAllItemProducts(token) {
    describe('GET productItems', function () {
        it('should get all productItems', function (done) {
            chai.request(app)
                .get('/productItems/')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });
}