//Use Request Promise to ask Firebase for the IDToken we need
const rp = require('request-promise');
const firebaseConfig = require('../firebaseConfig');
var admin = require('firebase-admin');

module.exports = {
    runTestWithIdToken : function(UID, test){
        admin.auth().createCustomToken(UID)
        .then(function(customToken) {
            var url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyCustomToken?key="+firebaseConfig.apiKey;

            var options = {
                "method": 'POST',
                "Content-Type" : "application/json",
                uri: url,
                body: {
                    "token" : customToken,
                    "returnSecureToken" : true
                },
                json: true // Automatically stringifies the body to JSON
            };
            
            rp(options)
            .then(function (parsedBody) {
                var idToken = parsedBody.idToken;
                test(idToken);
            })
            .catch(function (err) {
                console.log("Error creating ID token:", err);
            });   
        })
        .catch(function(error) {
            console.log("Error creating custom token:", error);
        });
    }
}