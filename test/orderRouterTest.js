var app = require('../app');

//Chai is an HTTP client appropriate for tests 
let chai = require('chai');
let chaiHttp = require('chai-http');
chai.use(chaiHttp);

const firebaseConfig = require('../firebaseConfig');
const firebaseService = require('./firebaseService');


firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testOrderCreation);
function testOrderCreation(token) {
    describe('POST Order', () => {
        it('should create an order', (done) => {
            var order = {
                "orderItems": [],
                "state": "SUBMITTED",
                "datePlaced": "2018-12-20T00:00:00.000Z",
                "deliveryAddress": {
                    "city": {
                        "name": "brussels",
                        "location": {
                            "latitude": 50.8462807,
                            "longitude": 4.3547273
                        }
                    }
                },
                "factory": {
                    "name": "porto",
                    "location": {
                        "latitude": 41.1621376,
                        "longitude": -8.6569732
                    }
                },
                "customer": "20"
            };

            chai.request(app)
                .post('/orders/')
                .set('Authorization', 'Bearer ' + token)
                .send(order)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    // body.distance.should.be.equal(expectedResult.distance);
                    // body.circuit.should.be.equal(expectedResult.circuit);
                    done();
                });
        });
    });
}

firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testGetOrder);
function testGetOrder(token) {
    describe('GET Order', function () {
        it('should get an order', function (done) {
            chai.request(app)
                .get('/orders/1')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });
}

firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testGetAllOrders);
function testGetAllOrders(token) {
    describe('GET Orders', function () {
        it('should get all orders', function (done) {
            chai.request(app)
                .get('/orders/')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });
}

firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testGetOrderHistory);
function testGetOrderHistory(token) {
    describe('GET OrderHistory', function () {
        it('should get OrderHistory', function (done) {
            chai.request(app)
                .get('/orders/OrderHistory')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });
}


firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testGetOrderHistoryByCustomer);
function testGetOrderHistoryByCustomer(token) {
    customerId: String;
    customerId = firebaseConfig.customerUID;
    describe('GET OrderHistory By CustomerId', function () {
        it('should get OrderHistory filtered By CustomerId', function (done) {
            chai.request(app)
                .get('/orders/OrderHistory/' + customerId)
                .set('Authorization', 'Bearer ' + token)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });
}

firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testGetOrderHistoryByCustomerAndState);
function testGetOrderHistoryByCustomerAndState(token) {
    customerId: String;
    customerId = firebaseConfig.customerUID;
    describe('GET OrderHistory By CustomerId and Order State', function () {
        it('should get OrderHistory filtered By CustomerId and Order State', function (done) {
            chai.request(app)
                .get('/orders/OrderHistory/' + customerId + '/SUBMITTED')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });
}

firebaseService.runTestWithIdToken(firebaseConfig.customerUID, testGetOrderHistoryByCustomerAndDate);
function testGetOrderHistoryByCustomerAndDate(token) {
    customerId: String;
    customerId = firebaseConfig.customerUID;
    describe('GET OrderHistory By CustomerId and Date Interval', function () {
        it('should get OrderHistory filtered By CustomerId and Date Interval', function (done) {
            chai.request(app)
                .get('/orders/OrderHistory/' + customerId + '/2019-01-01/2019-01-10')
                .set('Authorization', 'Bearer ' + token)
                .end((err, res, body) => {
                    res.status.should.be.equal(200);
                    done();
                });
        });
    });
}