var Order = require('../models/order');

module.exports = {
    findOrdersByState : async function (state){
        return await Order.find({state : state});
    },

    findByCustomer : async function (customer_id){
        return await Order.find({customer : customer_id});
    },

    findByCustomerAndState : async function (customer_id, state){
        return await Order.find({
            state: state,
            customer : customer_id
        });
    },

    findByCustomerAndDateInterval : async function (customer_id, date_init, date_end){
        return await Order.find({
            customer : customer_id,
            datePlaced: { "$gte": date_init, "$lte": date_end }
        });
    },
}