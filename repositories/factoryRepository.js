var Factory = require('../models/factory');

module.exports = {
    findAllFactories : async function (){
        return await Factory.find({});
    }
}