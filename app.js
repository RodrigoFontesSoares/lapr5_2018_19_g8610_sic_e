var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoose = require('mongoose');

var admin = require('firebase-admin');


admin.initializeApp({
  credential: admin.credential.cert({
    projectId: 'lapr5-74597',
    clientEmail: 'firebase-adminsdk-0pq24@lapr5-74597.iam.gserviceaccount.com',
    privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCnS5I/MFqD6gko\ns6/QflbEvkxnPgI6rt/Bx2PcxHfz9dy4mXJzrfypETWX/jJU8SfYVa7xelrSQCWZ\nrRojIFd/I3LhgH6Gv/xrXJqTL3q5Vys/BNou2UOvF0dadRN2x6xmOO3/oeAbn+EL\nt+PjG4KQDAOEmF46i2902FiR7jsfRUF/sdZQ/tMQJXRF7GRci6gKGPy6UzuISPxh\nhwkVOc2Tf67BJ3xZ+dwAQd2v0CiExnFQLa3XGzyIezhR5Bp0li7ZjQOF6y0T2Jfs\n3XWAlz756fszNe2cx06KnCOXTJ7oseALrVJ/WfEVDbRBbjmWXJ1Eq4tImfQO2cdT\nUIv9W6/bAgMBAAECggEAS7GFO8LwiXQYbcqTV4X6U21421OypYOfKSnCtM5OKdtJ\n4VpIby+/PG3Vbm5sroMokJkAHTFLq4qrmJM6Wr9U52kOHxrI5MYz+w28wLDAWeCn\naA2VfJIALlYdij+jrWAd0Gicwsemw0j9BZTP7t9xHSTaaKGaLOoO5XLH7Es6g+RS\nBtFFUdJmrHJlhsKrp/W6AW95IjG6USCQ3SwM2Rw9uY8TYJmK6A/eixLHmB/Yl3Yg\nI3GU8vooC7z5OVBkYzX9oWJTaFyKaDZcCC6IYtWnO9TqaRhWnPIFtZlxrTqmmSFp\n0wOwfOCFAQt3KpExFROpqG2tmwMAZ5om/8GoXvyDpQKBgQDZPHHXBv3RnMyAF3dF\nI9cdY7A8vpd9oQdUItbffHusncvSHMG6REmdhWKz4ZQg2/i4fNxL3QKxR3cfd4W7\nnnt8TCtio7EkgeMJhJOI02zJ1k1C5BnYHFO5YkdOJYIOznwzkHFgiqknOfU8QuJy\nS9P2qF3ME35NbrH3bFYrtHTX7QKBgQDFJccMHPQJ1tQsYPpkI+rMUFSnvml3TRnD\nPCV3v6BgGNrdEE5mjZ22MdKBYPD4RsUrd4j186nCw05K1DL5JurtIE//JSlRKDUI\n+65Li4RfiMjzZT5RVA2rSb22pdxDQkoyEecspQj4bYWDuyauSn9Ex/fcuAgY21Ch\nMT/QHHMd5wKBgCygPSaWGhgwZNY/e9HpPeycqPyhjo9RQCcpjQiWEvXv6In+PFHe\na1z/pLodMgfaCGslwz+WZFI0wyTkscVMFkP/hb4asG42fIRdBs3J2QRRkMeYAjMi\nBpUDunA0RlZyVpNlLdVORtd3EYjSmKAwMZfvGG2u53hSsB6K09zgkkw1AoGAaSRk\nq8klPbodHi36XD3rk1ukSnH6vW2ZN850DRFd7uJSl2OC7J7KZcjcg7O30DGuJihy\nGvoAp6aRssbJCKgA0sZQ47pl9qqC3m+b3R5jUrAYu/Za7fTt9VQ2oZqDhjtsZpe1\nutGpnxSKCF9fmcSRa5VaU5SyfM86iT1C0zLZEjECgYBVeMrjrJLNpvfcoYC1ZQ1t\nP8WU5wwCG1XLBeZPlKyXF3jn8zjpQEjPfhIqwczhhiGOJmgNNFxMurNLo+IizmqM\n/9Cfl73rmRP6TX7ZZ8pmbG4nqFsgbaHosCAcNrObO7mAyNsi2g6IgPWJ8b2KaAu3\n77oElhV7uPLiaWWn85iv4Q==\n-----END PRIVATE KEY-----\n'
  }),
  databaseURL: 'https://lapr5-74597.firebaseio.com'
});

//config file
var config = require('./config');
mongoose.connect(config.database, { useNewUrlParser: true }); // connect to our database

var indexRouter = require('./routes/index');
var productItemRouter = require('./routes/productItemRouter');
var orderRouter = require('./routes/orderRouter');
var orderManagerRouter = require('./routes/orderManagerRouter');

var app = express();

var port = process.env.PORT || 8080;
// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/productItems', productItemRouter);
app.use('/orders', orderRouter);
app.use('/orderManager', orderManagerRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port);
console.log('Magic happens on port ' + port);

module.exports = app;
